-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2019 at 05:32 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasumi`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(100) NOT NULL,
  `name_bank` varchar(255) DEFAULT NULL,
  `bank_code` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name_bank`, `bank_code`) VALUES
(1, 'mandiri', 1),
(2, 'bca', 2),
(3, 'mega', 3),
(4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id_bank` bigint(20) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id_bank`, `bank_name`, `branch_name`, `bank_code`, `created_at`, `updated_at`) VALUES
(1, 'mandiri', '', '1', NULL, NULL),
(2, 'bca', '', '2', NULL, NULL),
(3, 'bri', '', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` bigint(100) UNSIGNED NOT NULL,
  `name_branch` varchar(255) DEFAULT NULL,
  `bank_id` int(255) DEFAULT NULL,
  `branch_code` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name_branch`, `bank_id`, `branch_code`) VALUES
(1, 'mandiri syariah', 1, 1),
(2, 'mandiri konven', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_commuting_entry`
--

CREATE TABLE `detail_commuting_entry` (
  `id` bigint(10) UNSIGNED NOT NULL,
  `route_profile` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `transport` enum('car','train') DEFAULT NULL,
  `att_code` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `approve` enum('approve','pending','rejected') DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_commuting_entry`
--

INSERT INTO `detail_commuting_entry` (`id`, `route_profile`, `date`, `type`, `transport`, `att_code`, `purpose`, `route`, `approve`, `updated_at`, `created_at`, `path`) VALUES
(1, 'reza elang raharja', '2019-11-25', 'dinas', 'car', '33333', 'bertemu client', 'pulang', 'approve', '2019-11-29 07:39:29', '2019-11-25 11:35:23', '33333'),
(2, 'jakarta ke bandung', '2019-11-27', 'dolan', 'train', '44444', 'dolan', 'rumah ke jalan', 'approve', '2019-11-27 10:43:42', '2019-11-25 11:35:21', '444444'),
(3, 'dinas luar', '2019-11-12', 'dinas', 'car', '55555', 'bertemu reza khasbulla', 'depok to jakarta', '', '2019-11-27 09:21:09', '2019-11-25 11:35:16', '777777'),
(4, 'jakarta', '2019-11-06', 'jalan jalan', 'car', NULL, 'jalan jalan', 'jakarta ke bandung', 'rejected', '2019-11-27 10:26:05', '2019-11-26 11:02:30', '999999'),
(5, 'Jun Taslim', '2019-11-20', 'ttttt', 'train', NULL, 'wwwwww', 'kantor ke rumah', 'pending', '2019-11-28 04:11:16', '2019-11-26 11:03:04', 'wwwww'),
(6, 'depok', NULL, 'kpoop', 'car', NULL, 'hhhhh', 'kantor ke depok', 'pending', '2019-11-28 06:22:20', '2019-11-26 11:12:25', '666666'),
(7, 'jakarta ke magelang', NULL, 'dinas', NULL, NULL, 'depok ke tamanan', 'jakarta ke rumah', NULL, '2019-11-27 07:06:19', '2019-11-27 07:06:19', NULL),
(8, 'jakarta ke bandung', NULL, 'perjalanan dinas', NULL, NULL, 'dolan', 'rumah ke kantor', NULL, '2019-11-27 07:08:51', '2019-11-27 07:08:51', NULL),
(9, 'hilmy', NULL, 'dinas', 'car', NULL, 'dinas', 'dari nganjuk ke jakarta', 'approve', '2019-11-29 02:16:01', '2019-11-27 07:13:39', NULL),
(10, 'hilmy firdaus', NULL, 'dinas', NULL, NULL, 'dinas luar daerah', 'nganjuk ke jakarta', NULL, '2019-11-27 07:14:46', '2019-11-27 07:14:46', NULL),
(11, 'achmad hilmy', NULL, 'jalan jalan', NULL, NULL, 'dolana', 'dari rumah ke kantor', NULL, '2019-11-27 07:17:35', '2019-11-27 07:17:35', '555555555'),
(12, 'ashraf zubyr', NULL, 'dolan', NULL, NULL, 'dolan', 'ashraf', NULL, '2019-11-27 07:20:26', '2019-11-27 07:20:26', NULL),
(13, 'twice', NULL, 'dolan', 'car', NULL, 'okeee', 'twice to jakarta', 'approve', '2019-11-27 10:17:02', '2019-11-27 07:22:11', '0000000'),
(14, 'becakayu', '2019-03-06', 'jalan jalan', 'car', NULL, 'oke', 'tol ke senayan', 'pending', '2019-11-27 10:16:46', '2019-11-27 07:24:00', '888888'),
(15, 'hilmy', '2019-11-01', 'gosip', 'car', NULL, 'rapper', 'ttttttt', 'approve', '2019-11-27 10:16:52', '2019-11-27 07:25:33', '444444'),
(16, 'riska umi fs', '2019-11-08', 'perjalanan dinas', 'train', NULL, 'pekerjaan', 'rumah ke kantor', 'rejected', '2019-11-27 10:16:32', '2019-11-27 08:12:45', '11111111'),
(17, 'riska umi fs', '2019-11-05', 'dolan', 'car', NULL, 'jalan jalan', 'kediri ke jakarta', 'rejected', '2019-11-27 10:16:22', '2019-11-27 08:14:23', '111111'),
(18, 'tiffany hwang', '2019-11-07', 'konser', 'train', NULL, 'konser', 'dari korea ke indonesia', 'rejected', '2019-11-27 10:16:06', '2019-11-27 08:16:27', '333333333'),
(19, 'naruto', '2019-11-02', 'sensei', 'car', NULL, 'yuppp', 'konoha to sunagakure', 'pending', '2019-11-27 10:16:01', '2019-11-27 08:18:10', '9999999'),
(20, 'mbak uti', '2019-11-09', 'work', 'car', NULL, 'work', 'tanggerang to senayan', 'pending', '2019-11-27 10:11:00', '2019-11-27 08:46:49', '333333'),
(21, 'riska fs', '2019-10-31', 'dinas', 'train', NULL, 'jalan jalan dinas', 'kediri ke surabaya', 'pending', '2019-11-27 10:15:53', '2019-11-27 10:10:20', '888888'),
(22, 'bekasi', '2019-11-07', 'dinas', 'train', NULL, 'jalan jalan', 'becakayu', 'pending', '2019-11-27 10:42:27', '2019-11-27 10:41:34', '333333'),
(23, 'hilmy', '2019-11-05', 'iya', 'train', NULL, 'jalan jlaan', 'jakarta to bekasi', 'approve', '2019-11-29 06:50:03', '2019-11-29 06:46:49', '3333333');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id_employee` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `marital_status` enum('married','single','jomblo') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `commuting_means` enum('car','train') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dormitory_status` enum('home','kost','apartement') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expertise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id_employee`, `firstname`, `birthday`, `marital_status`, `postal_code`, `commuting_means`, `surname`, `gender`, `dormitory_status`, `expertise`, `created_at`, `updated_at`) VALUES
(1, '', '0000-00-00', NULL, 122222, NULL, NULL, NULL, NULL, NULL, '2019-11-19 02:03:01', '2019-11-19 02:03:01'),
(2, 'hilmyy', '2019-11-01', NULL, 333333, NULL, NULL, NULL, '', '13', '2019-11-19 02:07:15', '2019-11-19 02:07:15'),
(3, 'Reza', '2019-11-01', NULL, 333333, NULL, NULL, NULL, '', '123', '2019-11-19 02:13:25', '2019-11-19 02:13:25'),
(4, 'kawamatsu', '2019-11-09', NULL, 44444, NULL, 'jessica jung', NULL, '', '133', '2019-11-19 02:24:40', '2019-11-19 02:24:40'),
(5, 'kozuki', '2019-11-07', NULL, 333333, NULL, 'oden', NULL, '', '13', '2019-11-20 02:02:41', '2019-11-20 02:02:41'),
(6, 'kozuki sukiyaki', '2019-11-08', NULL, 122222, NULL, 'oden', NULL, '', '45', '2019-11-20 02:09:31', '2019-11-20 02:09:31'),
(7, 'jennie', '2019-11-02', NULL, 333333, 'car', 'jessica jung', NULL, '', '123', '2019-11-20 02:11:28', '2019-11-20 02:11:28'),
(8, 'uzumaki Naruto', '2019-11-28', NULL, 122222, NULL, 'jessica jung', 'male', '', '13', '2019-11-20 02:14:13', '2019-11-20 02:14:13'),
(9, 'Reza', '2019-11-22', NULL, 122222, NULL, 'jessica jung', 'female', '', '123', '2019-11-20 02:17:42', '2019-11-20 02:17:42'),
(10, 'kozuki', '2019-11-14', NULL, 333333, 'car', 'oden', 'male', '', '133', '2019-11-20 02:20:12', '2019-11-20 02:20:12'),
(11, 'lalisa manoban', '2019-11-08', 'married', 122222, 'car', 'jessica jung', 'male', '', '123', '2019-11-20 02:22:35', '2019-11-20 02:22:35'),
(12, 'kaisar tenji', '2019-11-30', 'jomblo', 122222, 'car', 'oden', 'male', 'apartement', '133', '2019-11-20 02:24:32', '2019-11-20 02:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_14_022244_create_recruitments_table', 1),
(4, '2019_11_14_084602_create_employees_table', 1),
(5, '2019_11_15_022026_create_banks_table', 1),
(6, '2019_11_19_033521_create_personal_usages_table', 2),
(7, '2019_11_25_023021_create_submissions_table', 3),
(8, '2019_11_30_161113_create_trips_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recruitments`
--

CREATE TABLE `recruitments` (
  `id_recruitment` bigint(20) UNSIGNED NOT NULL,
  `employee_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name_kanji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname_kanji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name_kana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname_kana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_area_addres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefecture_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` date NOT NULL,
  `bank_account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_semester_previus_company` tinyint(1) NOT NULL,
  `employee_clasification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_long_contract` tinyint(1) NOT NULL,
  `time_shift_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_working_time` time NOT NULL,
  `end_working_time` time NOT NULL,
  `days_work` int(11) NOT NULL,
  `tax_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic_hourly_salary` decimal(8,2) NOT NULL,
  `calculated_monthly_salary` decimal(8,2) NOT NULL,
  `employment_insurance_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_social_insurance` tinyint(1) NOT NULL,
  `pension_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_resume` tinyint(1) NOT NULL,
  `has_written_oath` tinyint(1) NOT NULL,
  `has_employee_agreement` tinyint(1) NOT NULL,
  `has_certificate_of_resident_card` tinyint(1) NOT NULL,
  `has_application_form_of_commuting_method` tinyint(1) NOT NULL,
  `has_compliance_agreement` tinyint(1) NOT NULL,
  `Has_health_check_report` tinyint(1) NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval_status` tinyint(1) NOT NULL,
  `approval_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recruitments`
--

INSERT INTO `recruitments` (`id_recruitment`, `employee_code`, `first_name_kanji`, `surname_kanji`, `first_name_kana`, `surname_kana`, `birthday`, `zip_code`, `sub_area_addres`, `city_address`, `prefecture_address`, `phone_number`, `department_code`, `join_date`, `bank_account`, `first_semester_previus_company`, `employee_clasification`, `is_long_contract`, `time_shift_category`, `start_working_time`, `end_working_time`, `days_work`, `tax_type`, `basic_hourly_salary`, `calculated_monthly_salary`, `employment_insurance_number`, `has_social_insurance`, `pension_number`, `has_resume`, `has_written_oath`, `has_employee_agreement`, `has_certificate_of_resident_card`, `has_application_form_of_commuting_method`, `has_compliance_agreement`, `Has_health_check_report`, `Description`, `approval_status`, `approval_time`, `created_at`, `updated_at`) VALUES
(1, '333333', 'kawamatsu', 'ninja', 'oniwabansu', 'oniwabansu', '2019-11-04', '77890', '', '', '', '09786767876', '', '0000-00-00', '', 0, '', 0, '', '00:00:00', '00:00:00', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00 00:00:00', NULL, NULL),
(2, '999999', 'denjiro', 'samurai', 'oniwabansu', 'oniwabansu', '2019-11-05', '77788', '', '', '', '081259022529', '', '0000-00-00', '', 0, '', 0, '', '00:00:00', '00:00:00', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00 00:00:00', NULL, NULL),
(3, '111', '111', '1111', '111', '111', '0000-00-00', '111', '111', '111', '111', '111', '111', '0000-00-00', '111', 111, '', 0, '', '00:01:11', '00:01:11', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 111, '0000-00-00 00:00:00', '2019-11-19 19:45:31', '2019-11-19 19:45:31'),
(4, '111', '111111', '11111', '1111', '111', '0000-00-00', '1111', '111', '111', '111', '111', '1111', '0000-00-00', '111', 111, '', 0, '', '00:01:11', '00:01:11', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 111, '0000-00-00 00:00:00', '2019-11-20 02:35:21', '2019-11-20 02:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(10) NOT NULL,
  `user_level` varchar(255) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_trip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destinasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `kode_trip`, `destinasi`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '555', 'aceh', 1, '2019-11-30 16:24:07', '2019-11-30 16:24:10'),
(2, '666', 'nganjuk', 3, '2019-11-29 16:24:32', '2019-11-30 16:24:35'),
(3, '777', 'kediri', 1, '2019-11-30 16:25:57', '2019-11-30 16:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `uploud`
--

CREATE TABLE `uploud` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `size` varchar(100) DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `update_at` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'hilmy', 'hilmy@gmail.com', NULL, '', NULL, '2019-11-25 04:49:58', '2019-11-25 04:50:01'),
(2, 'reza', 'reza@gmail.com', NULL, '', NULL, '2019-11-25 07:14:45', '2019-11-25 07:14:48'),
(3, 'ahmad', 'ahmad@gmail.com', NULL, '', NULL, '2019-11-25 07:15:10', '2019-11-25 07:15:13'),
(4, 'achmadhilmy', 'achmadhilmy@gmail.com', NULL, '$2y$10$cxuEWn821zhRftW/Pr2k4OE6kT9vLcMKgve40TjbTB3GB5GCvuKSe', NULL, '2019-11-27 20:57:11', '2019-11-27 20:57:11'),
(5, 'reza', 'reza@hampton.com', NULL, '$2y$10$YQuggF.qKpCJWe7Ft5rmMu0RPHzfLDC.wGBmNcf1QseX0CrPacG.e', NULL, '2019-11-27 21:10:30', '2019-11-27 21:10:30'),
(6, 'mbak uti', 'uti@gmail.com', NULL, '$2y$10$uV1B5LJzUhB/Dj5taAsspu2EvZ64JWPuTSBDvDN8sLANdB1s8HAo6', NULL, '2019-11-27 21:16:51', '2019-11-27 21:16:51'),
(7, 'citra', 'citra@gmail.com', NULL, '$2y$10$yUZOetT5A3xIODttMQ34bOTqn.VyUY73cGiFEVxIjPxD9BUUg9uYO', NULL, '2019-11-27 23:14:09', '2019-11-27 23:14:09'),
(8, 'kawamura', 'kawamura@kasumi.co.jp', NULL, '$2y$10$Dyydn4AJ5xYize.8HfSuo.D0aC7j0bIRoJ81cnzOa4ABVGXECATGy', NULL, '2019-11-28 21:16:25', '2019-11-28 21:16:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_commuting_entry`
--
ALTER TABLE `detail_commuting_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `recruitments`
--
ALTER TABLE `recruitments`
  ADD PRIMARY KEY (`id_recruitment`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploud`
--
ALTER TABLE `uploud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id_bank` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` bigint(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_commuting_entry`
--
ALTER TABLE `detail_commuting_entry`
  MODIFY `id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id_employee` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `recruitments`
--
ALTER TABLE `recruitments`
  MODIFY `id_recruitment` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `uploud`
--
ALTER TABLE `uploud`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
