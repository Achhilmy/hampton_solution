-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 03:04 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasumi`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(100) NOT NULL,
  `name_bank` varchar(255) DEFAULT NULL,
  `bank_code` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name_bank`, `bank_code`) VALUES
(1, 'mandiri', 1),
(2, 'bca', 2),
(3, 'mega', 3),
(4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id_bank` bigint(20) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id_bank`, `bank_name`, `branch_name`, `bank_code`, `created_at`, `updated_at`) VALUES
(1, 'mandiri', '', '1', NULL, NULL),
(2, 'bca', '', '2', NULL, NULL),
(3, 'bri', '', '3', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` bigint(100) UNSIGNED NOT NULL,
  `name_branch` varchar(255) DEFAULT NULL,
  `bank_id` int(255) DEFAULT NULL,
  `branch_code` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name_branch`, `bank_id`, `branch_code`) VALUES
(1, 'mandiri syariah', 1, 1),
(2, 'mandiri konven', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id_employee` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `marital_status` enum('married','single','jomblo') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `commuting_means` enum('car','train') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dormitory_status` enum('home','kost','apartement') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expertise` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id_employee`, `firstname`, `birthday`, `marital_status`, `postal_code`, `commuting_means`, `surname`, `gender`, `dormitory_status`, `expertise`, `created_at`, `updated_at`) VALUES
(1, '', '0000-00-00', NULL, 122222, NULL, NULL, NULL, NULL, NULL, '2019-11-19 02:03:01', '2019-11-19 02:03:01'),
(2, 'hilmyy', '2019-11-01', NULL, 333333, NULL, NULL, NULL, '', '13', '2019-11-19 02:07:15', '2019-11-19 02:07:15'),
(3, 'Reza', '2019-11-01', NULL, 333333, NULL, NULL, NULL, '', '123', '2019-11-19 02:13:25', '2019-11-19 02:13:25'),
(4, 'kawamatsu', '2019-11-09', NULL, 44444, NULL, 'jessica jung', NULL, '', '133', '2019-11-19 02:24:40', '2019-11-19 02:24:40'),
(5, 'kozuki', '2019-11-07', NULL, 333333, NULL, 'oden', NULL, '', '13', '2019-11-20 02:02:41', '2019-11-20 02:02:41'),
(6, 'kozuki sukiyaki', '2019-11-08', NULL, 122222, NULL, 'oden', NULL, '', '45', '2019-11-20 02:09:31', '2019-11-20 02:09:31'),
(7, 'jennie', '2019-11-02', NULL, 333333, 'car', 'jessica jung', NULL, '', '123', '2019-11-20 02:11:28', '2019-11-20 02:11:28'),
(8, 'uzumaki Naruto', '2019-11-28', NULL, 122222, NULL, 'jessica jung', 'male', '', '13', '2019-11-20 02:14:13', '2019-11-20 02:14:13'),
(9, 'Reza', '2019-11-22', NULL, 122222, NULL, 'jessica jung', 'female', '', '123', '2019-11-20 02:17:42', '2019-11-20 02:17:42'),
(10, 'kozuki', '2019-11-14', NULL, 333333, 'car', 'oden', 'male', '', '133', '2019-11-20 02:20:12', '2019-11-20 02:20:12'),
(11, 'lalisa manoban', '2019-11-08', 'married', 122222, 'car', 'jessica jung', 'male', '', '123', '2019-11-20 02:22:35', '2019-11-20 02:22:35'),
(12, 'kaisar tenji', '2019-11-30', 'jomblo', 122222, 'car', 'oden', 'male', 'apartement', '133', '2019-11-20 02:24:32', '2019-11-20 02:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_14_022244_create_recruitments_table', 1),
(4, '2019_11_14_084602_create_employees_table', 1),
(5, '2019_11_15_022026_create_banks_table', 1),
(6, '2019_11_19_033521_create_personal_usages_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_usage`
--

CREATE TABLE `personal_usage` (
  `id_personal_usage` bigint(20) UNSIGNED NOT NULL,
  `store_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_monthly_entry` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `submitting_commuting_app` timestamp(1) NULL DEFAULT NULL,
  `commuting_distance` decimal(8,2) DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefecture_addres` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_addres_and_block` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building_room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transportation_means` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_usage`
--

INSERT INTO `personal_usage` (`id_personal_usage`, `store_number`, `employee_number`, `current_monthly_entry`, `fullname`, `contract_type`, `submitting_commuting_app`, `commuting_distance`, `postal_code`, `prefecture_addres`, `street_addres_and_block`, `address_number`, `building_name`, `building_room_no`, `transportation_means`, `created_at`, `updated_at`) VALUES
(1, '11111', '2222', NULL, 'hhhhh', 'hhhhhh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-18 21:52:05', '2019-11-18 21:52:05'),
(2, '4444', '4444', '2019-11-19 07:19:55', 'hghghg', 'hghghg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 07:19:50', '2019-11-19 07:19:53'),
(3, '55555', '5555', '2019-11-19 07:20:52', 'gggggg', 'gggffff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 07:20:47', '2019-11-19 07:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `recruitments`
--

CREATE TABLE `recruitments` (
  `id_recruitment` bigint(20) UNSIGNED NOT NULL,
  `employee_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name_kanji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname_kanji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name_kana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname_kana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_area_addres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefecture_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` date NOT NULL,
  `bank_account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_semester_previus_company` tinyint(1) NOT NULL,
  `employee_clasification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_long_contract` tinyint(1) NOT NULL,
  `time_shift_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_working_time` time NOT NULL,
  `end_working_time` time NOT NULL,
  `days_work` int(11) NOT NULL,
  `tax_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic_hourly_salary` decimal(8,2) NOT NULL,
  `calculated_monthly_salary` decimal(8,2) NOT NULL,
  `employment_insurance_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_social_insurance` tinyint(1) NOT NULL,
  `pension_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_resume` tinyint(1) NOT NULL,
  `has_written_oath` tinyint(1) NOT NULL,
  `has_employee_agreement` tinyint(1) NOT NULL,
  `has_certificate_of_resident_card` tinyint(1) NOT NULL,
  `has_application_form_of_commuting_method` tinyint(1) NOT NULL,
  `has_compliance_agreement` tinyint(1) NOT NULL,
  `Has_health_check_report` tinyint(1) NOT NULL,
  `Description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval_status` tinyint(1) NOT NULL,
  `approval_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recruitments`
--

INSERT INTO `recruitments` (`id_recruitment`, `employee_code`, `first_name_kanji`, `surname_kanji`, `first_name_kana`, `surname_kana`, `birthday`, `zip_code`, `sub_area_addres`, `city_address`, `prefecture_address`, `phone_number`, `department_code`, `join_date`, `bank_account`, `first_semester_previus_company`, `employee_clasification`, `is_long_contract`, `time_shift_category`, `start_working_time`, `end_working_time`, `days_work`, `tax_type`, `basic_hourly_salary`, `calculated_monthly_salary`, `employment_insurance_number`, `has_social_insurance`, `pension_number`, `has_resume`, `has_written_oath`, `has_employee_agreement`, `has_certificate_of_resident_card`, `has_application_form_of_commuting_method`, `has_compliance_agreement`, `Has_health_check_report`, `Description`, `approval_status`, `approval_time`, `created_at`, `updated_at`) VALUES
(1, '333333', 'kawamatsu', 'ninja', 'oniwabansu', 'oniwabansu', '2019-11-04', '77890', '', '', '', '09786767876', '', '0000-00-00', '', 0, '', 0, '', '00:00:00', '00:00:00', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00 00:00:00', NULL, NULL),
(2, '999999', 'denjiro', 'samurai', 'oniwabansu', 'oniwabansu', '2019-11-05', '77788', '', '', '', '081259022529', '', '0000-00-00', '', 0, '', 0, '', '00:00:00', '00:00:00', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 0, '0000-00-00 00:00:00', NULL, NULL),
(3, '111', '111', '1111', '111', '111', '0000-00-00', '111', '111', '111', '111', '111', '111', '0000-00-00', '111', 111, '', 0, '', '00:01:11', '00:01:11', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 111, '0000-00-00 00:00:00', '2019-11-19 19:45:31', '2019-11-19 19:45:31'),
(4, '111', '111111', '11111', '1111', '111', '0000-00-00', '1111', '111', '111', '111', '111', '1111', '0000-00-00', '111', 111, '', 0, '', '00:01:11', '00:01:11', 0, '', '0.00', '0.00', '', 0, '', 0, 0, 0, 0, 0, 0, 0, '', 111, '0000-00-00 00:00:00', '2019-11-20 02:35:21', '2019-11-20 02:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_usage`
--
ALTER TABLE `personal_usage`
  ADD PRIMARY KEY (`id_personal_usage`);

--
-- Indexes for table `recruitments`
--
ALTER TABLE `recruitments`
  ADD PRIMARY KEY (`id_recruitment`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id_bank` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` bigint(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id_employee` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_usage`
--
ALTER TABLE `personal_usage`
  MODIFY `id_personal_usage` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `recruitments`
--
ALTER TABLE `recruitments`
  MODIFY `id_recruitment` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
