@extends('pages.home')
@section('content')

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<h2 class="text-center">Table Personal</h2>

<table>
  <tr>
    <th>Firstname</th>
    <th>Contact</th>
    <th>Approve</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
  <tr>  
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
  <tr>
    <td>Ernst Handel</td>
    <td>Roland Mendel</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
  <tr>
    <td>Island Trading</td>
    <td>Helen Bennett</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
  <tr>
    <td>Laughing Bacchus Winecellars</td>
    <td>Yoshi Tannamuri</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
  <tr>
    <td>Magazzini Alimentari Riuniti</td>
    <td>Giovanni Rovelli</td>
    <td><div class="col-sm-6">
          <input type="radio" name="gender" value="male"> Approve
          <input type="radio" name="gender" value="female"> Reject
    </div></td>
  </tr>
</table>

@endsection