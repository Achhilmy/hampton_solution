@extends('pages.home')
@section('content')

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<h2 class="text-center"> Employee Table</h2>
<div class="container">
  <div class="card">
    <div class="card-header">
            Featured
            <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 300px, height: 500px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
    </div>
    <div class="card-body table-responsive p-0" style="height: 500px;">
            <table class="table table-head-fixed">
              <thead>
                <tr>
                  <th>No数</th>
                  <th>First Name</th>
                  <th>Surname</th>            
                  <th>Dormitory</th>                        
                  <th>Postal Code </th>
                  <th>Actionアクション</th>

                </tr>
              </thead>
              <tbody>
                    
                @foreach ($employee as $index=>$employees)                
                    <tr>
                        @if(isset($_GET['page']))               
                            <td>{{(5*($_GET['page']-1))+$index+1}}</td>
                        @else
                            <td>{{$index+1}}</td>
                        @endif
                        <td>{{$employees->firstname}}</td>
                        <td>{{$employees->surname}}</td>   
                        <td>{{$employees->dormitory_status}}</td>   
                        <td>{{$employees->postal_code}}</td>                                      
                        <td>
                            <a href="/edit/{{$employees->id_personal_usage}}" class="btn btn-success">Accepted</a>&nbsp;
                            <a href="/delete/{{ $employees->id_personal_usage }}"class="btn btn-danger">Rejected</a>                           
                        </td>                                      

                    </tr> 
                @endforeach
                </tbody>
            </table>
    </div>
</div>

@endsection