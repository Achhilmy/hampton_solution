@extends('pages.home')
@section('content')

<style>
html, body {
  height: 100%;
}

.wrap {
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
}

.button {
  width: 140px;
  height: 45px;
  font-family: 'Roboto', sans-serif;
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 2.5px;
  font-weight: 500;
  color: #ffff;
  background-color: #ef4023;
  border: none;
  border-radius: 45px;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s;
  cursor: pointer;
  outline: none;
  }

.button:hover {
  background-color:#acacac;
  box-shadow: 0px 15px 20px rgba(0, 0, 0, 0.0));
  color: #ffff;

}
.nav-first.row {
    margin-top: -20px;
}
hr.nav-second {
    margin-bottom: 0px;
    margin-top: 0px;
}

label {
   font-size: 12px;
   margin-left:10px
}
p {
   font-size: 14px;
}
.wrap.center {
    margin-left: 460px;
    margin-bottom: 10px;
}

.hidden{
    display:none;
}
.example{
    float: right;
    margin-top : -30px;
}
{
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: auto;
  font-family: Raleway;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

</style>
    <div class="nav-first row">
      <div class="col-md-12 card ">
          <div class="card-body">
            <div class="col-md-12 inline" style="margin-top:10px; margin-bottom:-20px">
                 <div style="color:#ef4023; font-weight:bold; font-size:18px;">Commuter Submission Form </div>         
                    <form class="example" action="/action_page.php" >
                        <label>Store Number</label>
                        <input type="text" placeholder="Store Number" name="search2" style="border-radius:2px">
                        <button type="submit" style="color:#ef4023;"><i class="fa fa-search " style="color:#ef4023;"></i></button>
                        <label>Employee Number</label>
                        <input type="text" placeholder="Employee Number" name="search2">
                        <button type="submit"style="Backgound:#ef4023;"><i class="fa fa-search"style="color:#ef4023;"></i></button>
                    </form>
                 
            </div>     `                                        
          </div>
            <hr class="nav-second" size="30px"color ="#ef4023"/>
      </div>
    </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">              
                <label style="font-size:16px;">Terusty Harumdani</label>
                <label style="font-size:12px;">Tel : 099709809809</label>
                <label style="font-size:12px;">Menara Mandiri Cohive Lantai 11</label>
                <label style="font-size:12px;">Jakarta Selatan</label>

               
                <form id="regForm" action="/action_page.php">
                    <h1>Register:</h1>
                    <!-- One "tab" for each step in the form: -->
                    <div class="tab">Name:
                        <p><input placeholder="First name..." oninput="this.className = ''" name="fname"></p>
                        <p><input placeholder="Last name..." oninput="this.className = ''" name="lname"></p>
                    </div>
                    <div class="tab">Contact Info:
                        <p><input placeholder="E-mail..." oninput="this.className = ''" name="email"></p>
                        <p><input placeholder="Phone..." oninput="this.className = ''" name="phone"></p>
                    </div>
                    <div class="tab">Birthday:
                        <p><input placeholder="dd" oninput="this.className = ''" name="dd"></p>
                        <p><input placeholder="mm" oninput="this.className = ''" name="nn"></p>
                        <p><input placeholder="yyyy" oninput="this.className = ''" name="yyyy"></p>
                    </div>
                    <div class="tab">Login Info:
                        <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>
                        <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>
                    </div>
                    <div style="overflow:auto;">
                        <div style="float:right;">
                        <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                </form>
               .            
             
            
            

               

            
            </div>       
        </div>
    </div>
    
    

</div>
<div class="card card-primary">
                        <h3 class="text-center" style=" font-size:16px ; font-weight: bold; margin-top:10px">Detail Commuting Entry</h3>
                            <div class="card-header">
                                <label class="card-title">Datatable </label>
                                <a href="{{route('testingform.create')}}" class="btn btn-success  float-md-right modal-show" style="margin-top: -8px; font-size:14px " title="Create User"><i class="fas fa-plus"></i> Add New Commuting Trip</a>
                            </div>
                            <div class="card card-primary">
                                <table id="datatable" class="table table-hover" style="width:100%; font-size:12px ">
                                
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Route Profile</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Transport</th>
                                            <th>Attandence Code</th>
                                            <th>Route</th>
                                            <th>Approve</th>                                        
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    


                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Route Profile</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Transport</th>
                                            <th>Attandence Code</th>
                                            <th>Route</th>
                                            <th>Approve</th>  
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                
                                </table>
                            </div>
                    </div>

@endsection

@push('scripts')
  
    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{route('table.user') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'route_profile', name: 'route_profile'},
                {data: 'date', name: 'date'},
                {data: 'type', name: 'type'},
                {data: 'transport', name: 'transport'},
                {data: 'path', name: 'path'},               
                {data: 'route', name: 'route'},
                {data: 'approve', name: 'approve'},              
                {data: 'action', name: 'action'}
            ]
        });
    </script>

    <script>
        function ShowHideShowInfo(){
           
            $("#car").addClass('hide');
            if(document.getElementById('shipsame').checked){
                document.getElementById('car').style.display='block'; 
            }else{
                document.getElementById('car').style.display='none';
            }
        }
    
    </script>
        <script>
        function ShowHideShowtrain(){
           
            if(document.getElementById('trainsir').checked){
               
                document.getElementById('train').style.display='block'; 
            }else{
                
                document.getElementById('train').style.display='none';
            }
        }
    
    </script>
    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
        }

        function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
        }

        function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false
            valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
        }

        function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
        }
    </script>
   
@endpush