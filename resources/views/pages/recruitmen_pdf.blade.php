<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<div class="text-center">
		<h5>Membuat Laporan PDF Dengan DOMPDF Laravel</h4>
		<h6><a target="_blank" href="https://www.hamton-solutions/">www.hamton-solutions.com</a></h5>
	</div>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Firstname</th>
				<th>Surname</th>
				<th>PhoneNumber</th>
			
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($recruitment as $recruit)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$recruit->firstname_kanji}}</td>
				<td>{{$recruit->surname_kanji}}</td>
				<td>{{$recruit->phone_number}}</td>				
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>