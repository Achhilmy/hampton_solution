@extends('pages.home')
@section('content')

<h2 class="text-center">Employee Form</h2><br><br>


<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row ">
                <div class="col-6">
                             <!-- Horizontal Form -->
            <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Horizontal Form</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal">
                  <div class="card-body">
                    <div class="form-group row">
                    <label>SELECT BANK</label>
                      <select name="bank_name" id="bank_name" class="form-control input-lg dynamic" data-dependent="branch_name">
                          <option>Select Bank</option>
                          @foreach ($bank_list as $bank_name)
                          <option value="{{$bank_name -> bank_name}}">{{$bank_name->bank_name}}</option>                             
                          @endforeach
                      </select>
                    </div>
                    <div class="form-group row">
                            <label>Branch Name</label>
                              <select name="bank_name" id="bank_name" class="form-control input-lg dynamic" data-dependent="bank_code">
                                  <option>Branch name</option>
                              
                              </select>
                    </div>
                   
                    <div class="form-group row">
                        <label>BANK CODE</label>
                            <select name="bank_name" id="bank_name" class="form-control input-lg dynamic" >
                                <option>Code Bank</option>
                             
                            </select>
                    </div>
                    {{ csrf_field()}}
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-info">Save</button>
                    <button type="submit" class="btn btn-default float-right">Cancel</button>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>        
            </div>
            </div>
        </div>
    </div>
    
</div>

@endsection
<script type="text/javascript">
        var $ = jQuery;
        $(document).ready(function(){
        
         $('.dynamic').change(function(){
          if($(this).val() != '')
          {
           var select = $(this).attr("id_bank");
           var value = $(this).val();
           var dependent = $(this).data('dependent');
           var _token = $('input[name="_token"]').val();
           $.ajax({
            url:"{{ route('bank.fetch') }}",
            method:"POST",
            data:{select:select, value:value, _token:_token, dependent:dependent},
            success:function(result)
            {
             $('#'+dependent).html(result);
            }
        
           })
          }
         });
        
         $('#bank_name').change(function(){
          $('#branch_name').val('');
          $('#bank_code').val('');
         });
        
         $('#branch_name').change(function(){
          $('#bank_code').val('');
         });
         
        
        });
</script>
