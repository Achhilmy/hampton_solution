@extends('pages.home')
@section('content')
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<h2 class="text-center">Recruitment Table </h2>
<div class="col-12">
    <div class="card">
      <div class="card-header">
      <a href="/tablerecruitment/cetak_pdf" class="btn btn-primary" target="_blank">Export PDF</a>
      <a href="/tablerecruitment/cetak_csv" class="btn btn-success" target="_blank">Export CSV</a>

        <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 300px, height: 500px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 500px;">
        <table class="table table-head-fixed">
          <thead>
            <tr>
              <th>No数</th>
              <th>Employee Code</th>
              <th>FirstName漢字</th>
              <th>SurName漢字</th>                         
              <th>PhoneNumber電話番号</th>
              <th>Actionアクション</th>

            </tr>
          </thead>
          <tbody>
                
            @foreach ($recruitment as $index=>$recruitments)                
                <tr>
                    @if(isset($_GET['page']))               
                        <td>{{(5*($_GET['page']-1))+$index+1}}</td>
                    @else
                        <td>{{$index+1}}</td>
                    @endif
                    <td>{{$recruitments->employee_code}}</td>
                    <td>{{$recruitments->first_name_kanji}}</td>   
                    <td>{{$recruitments->surname_kanji}}</td>                                 
                    <td>{{$recruitments->phone_number}}</td>                 
                    <td>
                        <a href="/edit/{{$recruitments->id_recruitments}}" class="btn btn-success">Accepted</a>&nbsp;
                        <a href="/delete/{{ $recruitments->id_recruitments }}"class="btn btn-danger">Rejected</a>
                       
                    </td>                                      

                </tr>
            @endforeach
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>


@endsection
