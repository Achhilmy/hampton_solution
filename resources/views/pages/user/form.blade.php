{!! Form::model($model, [
    'route' => $model->exists ? ['testingform.update', $model->id] : 'testingform.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

    <div class="form-group">
        <label for="" class="control-label">Route profile</label>
        {!! Form::text('route_profile', null, ['class' => 'form-control', 'id' => 'route_profile']) !!}
    </div>

    <div class="form-group">
        <label for="" class="control-label">Date</label>
        {!! Form::date('date', null, ['class' => 'form-control', 'id' => 'date']) !!}
    </div>

    <div class="form-group">
        <label for="">Select Transport:</label>
        <select class="form-control" name="transport" >
            <option value="car">Car</option>
            <option value="train">Train</option>      
        </select>
    </div>
    

    <div class="form-group">
        <label for="" class="control-label">Route</label>
        {!! Form::text('route', null, ['class' => 'form-control', 'id' => 'route']) !!}
    </div>

    <div class="form-group">
        <label for="" class="control-label">Tipe</label>
        {!! Form::text('type', null, ['class' => 'form-control', 'id' => 'type']) !!}
    </div>

    <div class="form-group">
        <label for="" class="control-label">Attendence Code </label>
        {!! Form::text('path', null, ['class' => 'form-control', 'id' => 'path']) !!}
    </div>
    <div class="form-group">
        <label for="" class="control-label">Purpose</label>
        {!! Form::text('purpose', null, ['class' => 'form-control', 'id' => 'purpose']) !!}
    </div>

    <div class="form-group">
      <label for="" class="control-label">Approve</label>
      {!! Form::select('approve', [ 'approve' => 'Approve', 'pending' => 'Pending','rejected' => 'reject']);!!}
    </div>
    <input  type="checkbox" id="shipsame" onclick="ShowHideShowInfo()"><label>car</label>
    <input  type="checkbox" id="trainsir" onclick="ShowHideShowtrain()"><label>train</label>
        <div class="form-group hidden" id="car" name="bbak">
            <label>Car</label>&nbsp&nbsp&nbsp
            <input name="bbak" type="radio" checked><label>gasoline</label>
            <input name="bbak" type="radio"><label>pertamax</label>
            <input name="bbak" type="radio"><label>pertalite</label>
        <div class="card card-default ">
                <input>
                <input>
                <input>
                <input>
                <input>
                <input>
                <input>
            </div>
            
        </div>
        <div class="form-group hidden" id="train" >
        <label>Train</label>
        <div class="card card-default ">
            <input>
            <input>
            <input>
            <input>
            <input>
            <input>
            <input>
        </div>


  

{!! Form::close() !!}