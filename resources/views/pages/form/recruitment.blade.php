@extends('pages.home')
@section('content')

<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<style>
* {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: 50px auto;
  font-family: Raleway;
  padding: 40px;
  width: 90%;
  min-width: 300px;
  border-radius: 10px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
  border-radius: 10px;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
  border-radius: 10px;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
</style>
<body>

<form id="regForm" action = "{{route('pages.recruitment')}}" method="post">
    {{csrf_field()}}
  <h1>HR Recruitment:</h1>
  <!-- One "tab" for each step in the form: -->
  <div class="tab">Step 1:
  <br>
    <div class="col-md-12">    
      <div class="row">
        <div class="col-md-6 left">  
        <div class="form-group">
        <label for="id">First Name Kanji</label>
                <select id="id_bank" name="name_bank" class="form-control" >
                <option value="" selected disabled>Select</option>
                  @foreach($bank as $key => $banks)
                  <option value="{{$key}}"> {{$banks}}</option>
                  @endforeach
                  </select>
        </div>
        <div class="form-group">
                <label for="title">Select oi</label>
                <select name="branch" id="branch" class="form-control" >
                <option value="" selected disabled>Select</option>
                </select>
            </div>
          <div class="form-group">
            <label for="">First Name Kanji</label>
            <p><input placeholder="First Name Kanji" oninput="this.className = ''" name="first_name_kanji"></p>
          </div>
          <div class="form-group">
            <label for="">Surname Kanji</label>
            <p><input placeholder="Surname Kanji" oninput="this.className = ''" name="surname_kanji"></p>
          </div>
          <div class="form-group">
            <label for="">First Name Kana</label>
            <p><input placeholder="First Name Kana" oninput="this.className = ''" name="first_name_kana"></p>
          </div>
          <div class="form-group">
            <label for="">Surname Kana</label>
            <p><input placeholder="Surname Kana" oninput="this.className = ''" name="surname_kana"></p>
          </div>
        </div>
        <div class="col-md-6 right">
        <div class="form-group">
          <label for="">Birth Day</label>
          <p><input placeholder="Birth Day" oninput="this.className = ''" name="birthday"></p>
         </div>
         <div class="form-group">
            <label for="">Zip Code</label>
            <p><input placeholder="Zip Code" oninput="this.className = ''" name="zip_code"></p>
          </div>
          <div class="form-group">
            <label for="">Prefecture Adress</label>
            <p><input placeholder="Prefecture Adress" oninput="this.className = ''" name="prefecture_address"></p>
          </div>
          <div class="form-group">
            <label for="">Phone Number</label>
            <p><input placeholder="Phone Number" oninput="this.className = ''" name="phone_number"></p>
          </div>
          <div class="form-group">
            <label for="">Department Code</label>
            <p><input placeholder="Department Code" oninput="this.className = ''" name="department_code"></p>
          </div>  
        </div>
      </div>  
    </div>
  </div>
  <div class="tab">Step 2:
  <br>
  <div class="col-md-12">    
      <div class="row">
        <div class="col-md-6 left">  
          <label for="">Join Date</label>
          <p><input placeholder="Join Date" oninput="this.className = ''" name="join_date"></p>
          <label for="">Start Working Time</label>
          <p><input placeholder="Start Working Time" oninput="this.className = ''" name="start_working_time"></p>
          <label for="">End Working Time</label>
          <p><input placeholder="End Working Time" oninput="this.className = ''" name="end_working_time"></p>
          <label for="">Approval Status</label>
          <p><input placeholder="Approval Status" oninput="this.className = ''" name="approval_status"></p>
          <label for="">Approval Time</label>
          <p><input placeholder="Approval Time" oninput="this.className = ''" name="approval_time"></p>
        </div>
        <div class="col-md-6 right">
          <label for="">Bank Account</label>
          <p><input placeholder="Bank Account" oninput="this.className = ''" name="bank_account"></p>
          <label for="">First Semester Previous Company</label>
          <p><input placeholder="First Semester Previous Company" oninput="this.className = ''" name="first_semester_previus_company"></p>
          <label for="">Employee Clasification</label>
          <p><input placeholder="Employee Clasification" oninput="this.className = ''" name="employee_clasification"></p>
          <label for="">Is long Contract</label>
          <p><input placeholder="Is long Contract" oninput="this.className = ''" name="is_long_contract"></p>
          <label for="">Time Shift Category</label>
          <p><input placeholder="Time Shift Category" oninput="this.className = ''" name="time_shift_category"></p>
        </div>
        
      </div>
  </div>
   </div>
  <div class="tab">Step 3:
  <br>
  <div class="col-md-12">    
      <div class="row">
        <div class="col-md-6 left">
          <label for="">Sub Area Address</label>
          <p><input placeholder="sub_area_addres" oninput="this.className = ''" name="sub_area_addres"></p>
          <label for="">City Address</label>
          <p><input placeholder="city_address" oninput="this.className = ''" name="city_address"></p>
          <label for="">Tax Type</label>
          <p><input placeholder="Days Work" oninput="this.className = ''" name="tax_type"></p>
          <label for=""> basic_hourly_salary</label>
          <p><input placeholder="Tax Type" oninput="this.className = ''" name="basic_hourly_salary"></p>
          <label for="">calculated_monthly_salary</label>
          <p><input placeholder="Basic Hourly Salary" oninput="this.className = ''" name="calculated_monthly_salary"></p>
        </div>
        <div class="col-md-6 right">
          <label for="">employment insurance number</label>
          <p><input placeholder="Calculated Mothly Salary" oninput="this.className = ''" name="employment_insurance_number"></p>
          <label for="">has_social_insurance</label>
          <p><input placeholder="has_social_insurance" oninput="this.className = ''" name="has_social_insurance"></p>
          <label for="">pension_number</label>
          <p><input placeholder="pension_number" oninput="this.className = ''" name="pension_number"></p>
          <label for="">has_resume</label>
          <p><input placeholder="Pension Number" oninput="this.className = ''" name="has_resume"></p>
          <label for="">has_written_oath</label>
          <p><input placeholder="has_written_oath" oninput="this.className = ''" name="has_written_oath"></p>  
        </div>
      </div>
    </div>
    </div>
  <div class="tab">Step 4:
  <br>
  <div class="col-md-12">    
      <div class="row">
        <div class="col-md-6 left">
            <label for="">has_employee_agreement</label>
            <p><input placeholder="Has Writen Oath" oninput="this.className = ''" name="has_employee_agreement  "></p>
            <label for="">has_certificate_of_resident_card</label>
            <p><input placeholder="Has Employee agreement" oninput="this.className = ''" name="has_certificate_of_resident_card" ></p>
            <label for="">has_application_form_of_commuting_method</label>
            <p><input placeholder="Has Certivicate of resident card" oninput="this.className = ''" name="has_application_form_of_commuting_method"></p>
            <label for="">has_compliance_agreement</label>
            <p><input placeholder="Has Application form of commuting Method" oninput="this.className = ''" name="has_compliance_agreement" ></p>
        </div>
        <div class="col-md-6 left">
            <label for="">Has_health_check_report</label>
            <p><input placeholder="Has Compliance Agreement" oninput="this.className = ''" name="Has_health_check_report"></p>
            <label for="">Description</label>
            <p><input placeholder="Has Health Check Report" oninput="this.className = ''" name="Description" ></p>          
        </div>
      </div>
  </div>
       </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}


$('#bank_opt_id').change(function(){
    var id_bank = $(this).val();    
    if(id_bank){
        $.ajax({
           type:"GET",
           url:"{{url('get-branch-list')}}?bank_id="+id_bank,
           success:function(res){               
            if(res){
                $("#branch").empty();
                $("#branch").append('<option>Select</option>');
                //responseParsed = JSON.parse( res );
                console.log("res: " + res);
                $.each(res,function(key,value){
                  
                    $("#branch").append('<option value="'+key+'">'+ value+'</option>');
                });
           
            }else{
               $("#branch").empty();
            }
           }
        });
    }else{
        $("#branch").empty();
    }      
   });


</script>

</body>
</html>


@endsection