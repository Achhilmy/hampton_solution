@extends('pages.home')
@section('content')

<h2 class="text-center">Form Personal Car Usage</h2><br>

<div class="container">
    <div class="card">
    <div class="card-header">
         Featured
    </div>
        <div class="card-body">
            <div class="row ">
                <div class="col-md-12">
                <form class="" action="{{route('pages.form.personal')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6">
                        <div class="left">
                            <div class="form-group">
                                <label>Store Number</label>
                                <input type="text" class="form-control" placeholder="Store Number" name="store_number">
                            </div>
                            <div class="form-group">
                                <label>Employee Number</label>
                                <input type="text" class="form-control" placeholder="Employee Number" name="employee_number">
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" name="fullname">
                            </div>
                            <div class="form-group">
                                <label>Contact Type</label>
                                <input type="text" class="form-control" placeholder="Contact Type"name="contract_type">         
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="right">
                            <label>One Way Commuting Distance</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="In kilometer" name="">
                            </div>
                            <div class="col-md-2">
                                 <label>KM</label>
                            </div>

                            <label>Address</label>
                            <input type="text" class="form-control" placeholder="Choose Postal Code">

                            <label style="color:white;">Address</label>
                            <input type="text" class="form-control" placeholder="" disabled>

                            <label style="color:white;">Address</label>
                            <input type="text" class="form-control" placeholder="" disabled>                           
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                <label  style="color:white;" for="inputState">State</label>
                                <select id="inputState" class="form-control">
                                        <option selected>Choose...</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label  style="color:white;" for="inputState">State</label>
                                    <select id="inputState" class="form-control">
                                        <option selected>Choose...</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label  style="color:white;" for="inputZip">Zip</label>
                                    <select id="inputState" class="form-control">
                                        <option selected>Choose...</option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </div>                         
                            <label style="color:white;">Address</label>
                            <input type="text" class="form-control" placeholder="">

                            <label style="color:white;">Address</label>
                            <input type="text" class="form-control" placeholder="">

                            <label style="color:white;">Address</label>
                            <input type="text" class="form-control" placeholder="">

                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-10">I hereby declare that the data I entered is true  </div><br>
                        <div class="col-md-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck1">
                                    <label class="form-check-label" for="gridCheck1">                              
                                    Agree
                                    </label>
                            </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" value="save" Type="submit">Save</button> 
                            </div>                    
                        </div>           
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div><br>
@endsection