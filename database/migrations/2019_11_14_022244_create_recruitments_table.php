<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruitments', function (Blueprint $table) {
            $table->bigIncrements('id_recruitment');
            $table->string('employee_code');
            $table->string('first_name_kanji');
            $table->string('surname_kanji');
            $table->string('first_name_kana');
            $table->string('surname_kana');
            $table->date('birthday');
            $table->string('zip_code');
            $table->string('sub_area_addres');
            $table->string('city_address');
            $table->string('prefecture_address');
            $table->string('phone_number');
            $table->string('department_code');
            $table->date('join_date');
            $table->string('bank_account');
            $table->boolean('first_semester_previus_company');
            $table->String('employee_clasification');
            $table->boolean('is_long_contract');
            $table->String('time_shift_category');
            $table->time('start_working_time');
            $table->time('end_working_time');
            $table->integer('days_work');
            $table->string('tax_type');
            $table->decimal('basic_hourly_salary');
            $table->decimal('calculated_monthly_salary');            
            $table->string('employment_insurance_number');
            $table->boolean('has_social_insurance');
            $table->string('pension_number');
            $table->boolean('has_resume');
            $table->boolean('has_written_oath');
            $table->boolean('has_employee_agreement');
            $table->boolean('has_certificate_of_resident_card');
            $table->boolean('has_application_form_of_commuting_method');
            $table->boolean('has_compliance_agreement');
            $table->boolean('Has_health_check_report');
            $table->string('Description');
            $table->boolean('approval_status');        
            $table->dateTime('approval_time');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruitments');
    }
}
