<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Submission;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Uploud;
use App\Http\Requests;
use App\Trip;
use DB;

class TestingApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submission =Submission::first(); 
        $users =User::first();          
        return view('pages.testingsubmission', compact('users','submission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //make class create 

        $model = new User();
        return view ('pages.user.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi request

        $this->validate($request,[    
            'route_profile' =>'required|string|max:255', 
            'route' =>'required|string|max:255',
            
        ]);

        $model = Submission::create($request->all());
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Submission::findOrFail($id);
        return view('pages.user.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Submission::findOrFail($id);
        return view('pages.user.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [    
            'route_profile' =>'required|string|max:255',  
            'route' =>'required|string|max:255'. $id
        ]);
        $model = Submission::findOrFail($id);
        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Submission::findOrFail($id);
        $model->delete();
        // User::destroy($id);
    }

    // public function dataTable()
    // {
    //     $model = Submission::query();
    //     return DataTables::of($model)
    //         ->addColumn('action', function ($model) {
    //             return view('pages.layouts._actions', [
    //                 'model' => $model,
    //                 'url_show' => route('testingform.show', $model->id),
    //                 'url_edit' => route('testingform.edit', $model->id),
    //                 'url_destroy' => route('testingform.destroy', $model->id)
    //             ]);
    //         })
    //         ->addIndexColumn()
    //         ->rawColumns(['actions'])
    //         ->make(true);
    // }

        public function dataTable()
        {
            $model = Submission::query();
            return DataTables::of($model)
                ->addColumn('action', function ($model) {
                    return view('pages.layouts._actions', [
                        'model' => $model,
                        'url_show' => route('testingform.show', $model->id),
                        'url_edit' => route('testingform.edit', $model->id),
                        'url_destroy' => route('testingform.destroy', $model->id)
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }


    public function uploudfile(Request $request){
       $path = $request->file('path')->store('pdf');

       //dd($path);
    //     $request->uploud()->uploudfile([
    //        'path' => $path
    //    ]);

       $request->uploud()->uploudfile([
           'path' => $path
       ]);
       return redirect()->back();

    }

    public function getdata()    
    {
    // $trip = Trip::groupBy('user_id')
    //            ->select('user_id', DB::raw('count(*) as total_trip'))
    //            ->get();
    // return view('pages.form.changepersonal',compact('trip'));
            
    // dd($trip);

    $trip = Trip :: groupBy('user_id')
                 ->select('user_id', DB::raw('count(*) as total_trip'))
                 ->get();;
    return view('pages.form.changepersonal',compact('trip'));

    //dd($user);
    //return view('pages.form.changepersonal', compact('trip'));
    
    //return view('pages.form.changepersonal',['trip'=>$trip]);
    }

}
