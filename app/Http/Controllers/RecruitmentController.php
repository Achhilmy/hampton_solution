<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
use DB;
use App\Quotation;
use PDF;
use App\Exports\RecruitmentExport;
use Maatwebsite\Excel\Facades\Excel;

class RecruitmentController extends Controller
{
    public function exportcsv() 
    {
        return Excel::download(new RecruitmentExport, 'Recruitment.csv');
    }
    public function getbank(){
        $bank_list = DB::table('banks')
                            ->groupBy('bank_name')
                            ->get();
        return view ('pages.bank')->with('bank_list'
                    ,$bank_list);
    }

    function fetch(Request $request)
    {
     $select = $request->get('select');
     $value = $request->get('value');
     $dependent = $request->get('dependent');
     $data = DB::table('banks')
       ->where($select, $value)
       ->groupBy($dependent)
       ->get();
     $output = '<option value="">Select '.ucfirst($dependent).'</option>';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
     }
     echo $output;
    }
    //method untuk menampilkan data 
    public function index(){  
        
      
        $recruitment = Recruitment::latest()->paginate('10');
        //dd($work_orders);
        return view('pages.tbl_recruitment', compact('recruitment'));
    }
    //untuk menampilkan form data 
    public function create()
    {
        $bank = DB::table("bank")->pluck("name_bank","bank_code");
        return view('pages.form.recruitment', compact('bank'));
        
    }

    public function getBranchList(Request $request)
    {
        $branch = DB::table("branch")
        ->where("bank_id",$request->bank_id)->pluck("name_branch","id");
        dd($branch);
        return response()->json($branch);
    }   

    //menyimpan data dari form ke database
    public function store()
    {
        Recruitment::create([
            'employee_code'=> request('employee_code'),
            'first_name_kanji'=> request('first_name_kanji'),
            'surname_kanji'=> request('surname_kanji'),
            'first_name_kana'=> request('first_name_kana'),
            'surname_kana'=> request('surname_kana'),
            'birthday'=> request('birthday'),
            'zip_code'=> request('zip_code'),
            'sub_area_addres'=> request('sub_area_addres'),
            'city_address'=> request('city_address'),
            'prefecture_address'=> request('prefecture_address'),
            'phone_number'=> request('phone_number'),
            'department_code'=> request('department_code'),
            'join_date'=> request('join_date'),
            'end_working_time'=> request('end_working_time'),
            'start_working_time'=> request('start_working_time'),
            'approval_status'=> request('approval_status'),
            'approval_time'=> request('approval_time'),
            'bank_account'=> request('bank_account'),
            'first_semester_previus_company'=> request('first_semester_previus_company'),
            
            
            
        ]); 
        return redirect ('/recruitment');
    }

    public function cetak_pdf()
    {
    	$recruitment = Recruitment::all();
 
    	$pdf = PDF::loadview('pages.recruitmen_pdf',['recruitment'=>$recruitment]);
    	return $pdf->download('laporan-recruitment.pdf');
    }
}
