<?php
use Maatwebsite\Excel\Facades\Excel;
namespace App\Http\Controllers;
use App\Exports\PersonalExport;
use Illuminate\Http\Request;
use App\PersonalUsage;
use DB;
use App\Quotation;

use Excel;


class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function exportcsv(){
        return Excel::download (new PersonalExport, 'personal.csv');
        
    }

    public function exportxls(){
        $personal_data = DB::table('personal_usage')->get()->toArray();
        $personal_array[] =array('Store Number','Employee Number', 'Fullname', 'Contract Type');
        foreach($personal_data as $personal){
            $personal_array[] = array(
                'Store Number' => $personal->store_number,
                'Employee Number' => $personal ->employee_number,
                'Fullname' =>$personal->fullname,
                'Contract Type' =>$personal->contract_type
            );
        }
        Excel::create('Personal Data ', function($excel) use ($personal_array){
            $excel->setTitle('Employee Number');
            $excel->sheet('Employee Number', function ($sheet) use ($personal_array){
                $sheet->fromArray($personal_array, null, 'A1',false, false);
            });
        })->download('xls');

    } 
    public function index(){
        return view ('pages.form.personal');
    }

    public function getview()
    {
        //menampilkan data 
        $personal_usage = PersonalUsage::latest()->paginate('10');
        
        return view('pages.tbl_personal', compact('personal_usage'));
    }
    /**

     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        PersonalUsage::create([
            'store_number'=> request('store_number'),
            'employee_number'=> request('employee_number'),
            'current_monthly_entry'=> request('current_monthly_entry'),
            'fullname'=> request('fullname'),
            'contract_type'=> request('contract_type'),
            'submitting_commuting_app'=> request('submitting_commuting_app'),
            'commuting_distance'=> request('commuting_distance'),
            'postal_code'=> request('postal_code'),
            'prefecture_addres'=> request('prefecture_addres'),
            'street_addres_and_block'=> request('street_addres_and_block'),
            'address_number'=> request('address_number'),
            'building_name'=> request('building_name'),
            'building_room_no'=> request('building_room_no'),
            'transportation_means'=> request('transportation_means'),
            
        ]); 
        return redirect ('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
