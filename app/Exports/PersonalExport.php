<?php

namespace App\Exports;

use App\PersonalUsage;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PersonalExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array {
        return [
                'Employee Number', 'Store Number', 'Fullname', 'Contract Type',
            ]; 
        
    }
    public function collection()
    {
        return PersonalUsage::select(['employee_number',
        'store_number','fullname','contract_type'])->get();
    }

}
