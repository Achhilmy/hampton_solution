<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    //
    public $table ="detail_commuting_entry";

    protected $fillable =['route_profile','date','type','transport','attendance_code','purpose','route','approve','path'];

}
