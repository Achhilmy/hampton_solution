<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruitment extends Model
{
    //menginsert data ke database dan juga menampilkan data dari mysql 
    protected $fillable =['employee_code','first_name_kanji','surname_kanji','first_name_kana',
    'surname_kana','birthday','zip_code','sub_area_addres','city_address', 'prefecture_address','phone_number','department_code','approval_time','approval_status','start_working_time','end_working_time','join_date','bank_account','first_semester_previus_company'];
}
