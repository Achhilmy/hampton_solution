<?php

namespace App;
use App\User;
use App\Trip;


use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    //
    protected $table='trips';

    protected $fillable = [
        'kode_trip', 'destinasi', 'user_id',
    ];
    public function user(){
        return $this ->belongsTo('App\User', 'user_id', 'id');
    }
    
}
