<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    

        public $table = "employee";
        
    //menginsert data ke database dan juga menampilkan data dari mysql 
        protected $fillable =['firstname','birthday','postal_code','commuting_means',
        'surname','gender','dormitory_status','expertise','marital_status'];
    
}
