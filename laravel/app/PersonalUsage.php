<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Excel;

class PersonalUsage extends Model
{

    public $table = "personal_usage";
    
//menginsert data ke database dan juga menampilkan data dari mysql 
    protected $fillable =['store_number','employee_number','current_monthly_entry','fullname',
    'contract_type','submitting_commuting_app','zip_code','commuting_distance','postal_code', 'prefecture_addres','street_addres_and_block','address_number',
    'building_name','building_room_no','transportation_means'];

 
}
