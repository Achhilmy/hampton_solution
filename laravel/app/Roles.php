<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public $table ="roles";

    protected $fillable =['user_level','user_id'];

    public function user(){
        return $this ->belongsTo('App\User', 'user_id','id');
    }


}
