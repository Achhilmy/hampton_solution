<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Submission;
use App\User;
use DB;
use App\Quotation;
use yajra\Datatables\Datatables;

class SubmissionFormController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getview()
    {
        //menampilkan data 
        $detail_commuting_entry = Submission::latest()->paginate('10');
        
        return view('pages.commutersubmissionform', compact('detail_commuting_entry'));
    }

     public function index()
    {
        $users =User::first();    
        return view('pages.home', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('pages.testingsubmission');
    }

    /**
     * Store a newly created resource in storage.
     *                                                      
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   
}
