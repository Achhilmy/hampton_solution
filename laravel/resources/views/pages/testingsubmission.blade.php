@extends('pages.home')
@section('content')

<style>
html, body {
  height: 100%;
}

.wrap {
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
}

.button {
  width: 140px;
  height: 45px;
  font-family: 'Roboto', sans-serif;
  font-size: 11px;
  text-transform: uppercase;
  letter-spacing: 2.5px;
  font-weight: 500;
  color: #ffff;
  background-color: #ef4023;
  border: none;
  border-radius: 45px;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s;
  cursor: pointer;
  outline: none;
  }

.button:hover {
  background-color:#acacac;
  box-shadow: 0px 15px 20px rgba(0, 0, 0, 0.0));
  color: #ffff;

}
.nav-first.row {
    margin-top: -20px;
}
hr.nav-second {
    margin-bottom: 0px;
    margin-top: 0px;
}

label {
   font-size: 12px;
   margin-left:10px
}
p {
   font-size: 14px;
}
.wrap.center {
    margin-left: 460px;
    margin-bottom: 10px;
}

.hidden{
    display:none;
}
</style>
    <div class="nav-first row">
      <div class="col-md-12 card ">
          <div class="card-body">
            <div class="col-md-8 inline">
                 <div style="color:#ef4023; font-weight:bold; font-size:18px;">Commuter Submission Form </div>              
            </div>     
          </div>
            <hr class="nav-second" size="30px"color ="#ef4023"/>
      </div>
    </div>

<div class="container">
    <div class="card">
    <div class="card-header">
        <div class="row">
           <label> Commuter Submission </label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
        
        </div>
    </div>  
    <div class="card-body">
            <div class="row ">
                <div class="col-md-12">
                <form class="" action="{{route('change.personal')}}" method="get">
                    {{csrf_field()}} 
                   <div class="row">
                        <div class="col-md-6">
                        <div class="left">                           
                            <div class="form-group">
                                        <label>Store Number</label>
                                        <select class="form-control" name="marital_status" id="marital_status">
                                            <option value="Tidak Ada" disable>Select One</option>
                                            <option name="marital_status" value="married">Office1 </option>
                                            <option name="marital_status" value="single">Office2</option>     
                                            <option name="marital_status" value="jomblo">Office3 </option>                                
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" placeholder="Full Name" disabled name="fullname">
                                    </div>
                                    <div class="form-group">
                                    <label>Submitted Commuting Application this month</label><br>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input " name="optradio">Yes
                                            </label>
                                            </div>
                                            <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="optradio">No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <label >Address</label>
                                            <input type="text" class="form-control" disabled placeholder="Contact Type"name="Address">         
                                    </div>
                                    <div class="form-group">
                                            <label style="color:white;">Address</label>
                                            <input type="text" class="form-control" disabled placeholder="Contact Type"name="Address">         
                                    </div>
                                    <div class="form-group">
                                            <label style="color:white;">Address</label>
                                            <input type="text" class="form-control" disabled placeholder="Contact Type"name="Address">         
                                    </div>
                                    <div class="form-group">
                                        <label style="color:white;">Address</label>
                                        <input type="text" class="form-control" placeholder="" disabled>    
                                    </div>                       
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                        <label  style="color:white;" for="inputState">State</label>
                                        <select id="inputState"disabled class="form-control">
                                                <option selected>Choose...</option>
                                                <option>...</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label  style="color:white;" for="inputState">State</label>
                                            <select id="inputState" disabled class="form-control">
                                                <option selected>Choose...</option>
                                                <option>...</option>
                                            </select>
                                        </div>                                                                  
                                        <div class="form-group col-md-4">
                                            <label  style="color:white;" for="inputZip">Zip</label>
                                            <select id="inputState"disabled class="form-control">
                                                <option selected>Choose...</option>
                                                <option>...</option>
                                            </select>
                                        </div>
                                    </div>                         
                                    <label style="color:white;">Address</label>
                                    <input type="text" class="form-control" disabled placeholder="">

                                    <label style="color:white;">Address</label>
                                    <input type="text" class="form-control" disabled placeholder="">
                            </div><br><br>
                        </div>
                        <div class="col-md-6">
                            <div class="right">                         
                                    <div class="form-group">
                                        <label>Employee Number</label>
                                        <select class="form-control" name="marital_status" id="marital_status">
                                            <option value="Tidak Ada" disable>Select One</option>
                                            <option name="marital_status" value="married">Permanent Staf</option>
                                            <option name="marital_status" value="single">Hr Staff</option>     
                                            <option name="marital_status" value="jomblo">Jomblo </option>                                
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Type</label>
                                        <input type="text" class="form-control" placeholder="Contact Type" disabled name="contract_type">         
                                    </div>
                                
                                    <!-- <div class="form-group card">
                                            <label >Distance from Home to Office</label>
                                            <input type="text" class="form-control"disabled placeholder="Contact Type" name="car_insurance">  
                                            <form action="{{route('testing.uploud')}}" method="post" enctype="multipart/form-data">
                                                {{csrf_field()}} 
                                                <input type="file" name="path"><br><br>
                                                <button class="btn btn-success" type="submit">Submit</button>           
                                            </form>       
                                    </div> -->
                            
                                    <div class="form-group card">
                                            <label class="text-center" >Car Insurance Expired</label>
                                            <input type="text" class="form-control"disabled placeholder="Contact Type" name="car_insurance">  
                                            <form action="{{route('testing.uploud')}}" method="post" enctype="multipart/form-data">
                                                {{csrf_field()}} 
                                                <input class="text-center" type="file" name="path"><br>
                                                
                                            </form>       
                                    </div>
                                    <br><br>
                                    <div class="form-group card">                                        
                                            <label class="text-center" >Car Insurance Expired</label>
                                            <input type="text" class="form-control"disabled placeholder="Contact Type" name="car_insurance">  
                                            <form action="{{route('testing.uploud')}}" method="post" enctype="multipart/form-data">
                                                {{csrf_field()}} 
                                                <input class="text-center" type="file" name="path"><br>
                                                                                                
                                            </form>   
                                         
                                    </div>
                                    
                                    <br><br>

                            </div>
                        </div>
                            <div class="wrap center">
                                <a href="{{'change.personal'}}"><button class="button ">Next</button></a><br>
                            </div>

                    </div>
                </form>



                    </div>
            </div>
        </div>
    </div>

   
</div><br>

<div class="card card-primary">
                        <h3 class="text-center" style=" font-size:16px ; font-weight: bold; margin-top:10px">Detail Commuting Entry</h3>
                            <div class="card-header">
                                <label class="card-title">Datatable </label>
                                <a href="{{route('testingform.create')}}" class="btn btn-success  float-md-right modal-show" style="margin-top: -8px; font-size:14px " title="Create User"><i class="fas fa-plus"></i> Add New Commuting Trip</a>
                            </div>
                            <div class="card card-primary">
                                <table id="datatable" class="table table-hover" style="width:100%; font-size:12px ">
                                
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Route Profile</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Transport</th>
                                            <th>Attandence Code</th>
                                            <th>Route</th>
                                            <th>Approve</th>                                        
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    


                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Route Profile</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Transport</th>
                                            <th>Attandence Code</th>
                                            <th>Route</th>
                                            <th>Approve</th>  
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                
                                </table>
                            </div>
                    </div>

@endsection

@push('scripts')

    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{route('table.user') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'route_profile', name: 'route_profile'},
                {data: 'date', name: 'date'},
                {data: 'type', name: 'type'},
                {data: 'transport', name: 'transport'},
                {data: 'path', name: 'path'},               
                {data: 'route', name: 'route'},
                {data: 'approve', name: 'approve'},              
                {data: 'action', name: 'action'}
            ]
        });
    </script>

    <script>
        function ShowHideShowInfo(){
           
            $("#car").addClass('hide');
            if(document.getElementById('shipsame').checked){
                document.getElementById('car').style.display='block'; 
            }else{
                document.getElementById('car').style.display='none';
            }
        }
    
    </script>
        <script>
        function ShowHideShowtrain(){
           
            if(document.getElementById('trainsir').checked){
               
                document.getElementById('train').style.display='block'; 
            }else{
                
                document.getElementById('train').style.display='none';
            }
        }
    
    </script>


@endpush