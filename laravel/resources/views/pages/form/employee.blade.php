@extends('pages.home')
@section('content')
<h2 class="text-center">Form Employee</h2><br>
<div class="container">
    <div class="card">
    <div class="card-header text-center">
         Form Employee
    </div>
        <div class="card-body">
            <div class="row ">
                <div class="col-md-12">
                <form class="" action="{{route('pages.form.employee')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <div class="form-group">
                                    <label>Firstname Kanji</label>
                                    <input type="text" class="form-control" placeholder="Firstname" name="firstname">
                                </div>
                                <div class="form-group">
                                    <label>Birthday</label>                            
                                    <input name="birthday" type="date" class="datepicker form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Marital Status</label>
                                    <select class="form-control" name="marital_status" id="marital_status">
                                        <option value="Tidak Ada" disable>Select One</option>
                                        <option name="marital_status" value="married">Married</option>
                                        <option name="marital_status" value="single">Single</option>     
                                        <option name="marital_status" value="jomblo">Jomblo </option>                                
                                    </select>
                                </div>
                                <div class="form-group">                        
                                    <label>Postal Code</label>
                                    <input type="text" class="form-control" placeholder="Firstname" name="postal_code">
                                </div>
                                <div class="form-group">
                                    <label>Commuting Means</label>
                                    <select class="form-control" name="commuting_means" id="commuting_means">
                                        <option disable>Select One</option>
                                        <option name="commuting_means" value="car">Car</option>
                                        <option name="commuting_means" value="train">Train</option>                                                               
                                    </select>
                                </div>                           
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right">
                            <div class="form-group">
                                    <label>Firstname Kanji</label>
                                    <input type="text" class="form-control" placeholder="Surname" name="surname">
                                </div>
                                <div class="form-group" style="margin-bottom:22px; " name="gender">
                                    <label>Gender</label><br>
                                    <label class="radio-inline">
                                         <input type="radio" name="gender" value="male" checked>Male
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="female">Female
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Dormitory Status</label>
                                    <select class="form-control" name="dormitory_status" id="dormitory_status">
                                        <option value="Tidak Ada" disable>Select One</option>
                                        <option name="dormitory_status" value="home">Home</option>
                                        <option name="dormitory_status" value="kost">Kost</option>     
                                        <option name="dormitory_status" value="apartement">Apartement </option>                                
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Expertise</label>
                                    <input type="text" class="form-control" placeholder="" name="expertise" > 
                                </div>                          
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8">I hereby declare that the data I entered is true  </div><br>
                                 <div class="col-md-8">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="gridCheck1">
                                        <label class="form-check-label" for="gridCheck1">                              
                                        Agree
                                        </label>
                                    </div>
                                </div>
                                <a class="btn btn-danger" href="{{url('/home')}}">Back</a>&nbsp;&nbsp;
                                <button class="btn btn-success" value="save" Type="submit">Save</button>                  
                            </div>           
                        </div>
                </form><br> 
            </div>
        </div>
    </div>
</div><br><br>
@endsection