@extends('pages.home')
@section('content')

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<h2 class="text-center"> Personal Table</h2>

<div class="container">
  <div class="card">
    <div class="card-header">         
            <a href="{{route('export_excel.excel')}}" class="btn btn-primary" target="_blank">Export Csv</a>
            
            <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 300px, height: 500px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
    </div>

    <div class="card-body table-responsive p-0" style="height: 500px;">
        <table class="table table-head-fixed">
          <thead>
            <tr>
              <th>No数</th>
              <th>Store number Code</th>
              <th>Employee Number</th>            
              <th>FullName</th>                        
              <th>Currently Month Enter </th>
              <th>Actionアクション</th>

            </tr>
          </thead>
          <tbody>
                
            @foreach ($personal_usage as $index=>$personals)                
                <tr>
                    @if(isset($_GET['page']))               
                        <td>{{(5*($_GET['page']-1))+$index+1}}</td>
                    @else
                        <td>{{$index+1}}</td>
                    @endif
                    <td>{{$personals->store_number}}</td>
                    <td>{{$personals->employee_number}}</td>   
                    <td>{{$personals->fullname}}</td>   
                    <td>{{$personals->created_at}}</td>                                      
                    <td>
                        <a href="/edit/{{$personals->id_personal_usage}}" class="btn btn-success">Accepted</a>&nbsp;
                        <a href="/delete/{{ $personals->id_personal_usage }}"class="btn btn-danger">Rejected</a>
                        
                    </td>                                      

                </tr> 
            @endforeach
            </tbody>
        </table>









  </div>
</div>
@endsection