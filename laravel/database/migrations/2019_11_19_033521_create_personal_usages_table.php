<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_usages', function (Blueprint $table) {
            $table->bigIncrements('id_personal_usage');
            $table->string('store_number');
            $table->string('employee_number');
            $table->timestamp('current_monthly_entry');
            $table->string('fullname');
            $table->string('contract_type');
            $table->boolean('submitting_commuting_app');
            $table->decimal('commuting_distance');
            $table->string('postal_code');
            $table->string('prefecture_addres');
            $table->string('street_addres_and_block');
            $table->string('address_number');
            $table->string('building_name');
            $table->string('building_room_no');
            $table->string('transportation_means');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_usages');
    }
}
