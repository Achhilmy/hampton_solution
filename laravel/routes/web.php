<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use App\Trip;

Route::get('/', function () {
    return view('welcome');
});

//route untuk form 
Route::get('/register', 'AuthController@getRegister')->name('register')->middleware('guest');
Route::post('/register', 'AuthController@postRegister')->middleware('guest');
Route::get('/login', 'AuthController@getLogin')->name('login')->middleware('guest');
Route::post('/login', 'AuthController@postLogin')->middleware('guest');
Route::get('/logout','AuthController@logout')->middleware('auth')->name('logout');
//end form
Route::get('/home','AuthController@index')->name('pages.home')->middleware('auth'); ;
Route::get('/tbl_employee','EmployeeController@index')->name('pages.form.employee')->middleware('auth'); ;
Route::get('/personalcarusage', function () { return view('pages.personal');})->middleware('auth'); ; 
//clue form 

Route::get('/cluepersonal', function () { return view('pages.clue.cluepersonal');})->middleware('auth'); 
Route::get('/changepersonal', 'testingApiController@getdata')->name('change.personal');

//form input
Route::get('/formpersonal', 'PersonalController@index')->name('pages.form.personal')->middleware('auth');
Route::post('/formpersonal', 'PersonalController@store')->name('pages.form.personal')->middleware('auth');  
Route::get('/viewpersonal', 'PersonalController@getview')->name('pages.tbl_personal')->middleware('auth');
Route::get('/submissionform', 'SubmissionFormController@index')->name('pages.commutersubmissionform')->middleware('auth');
Route::get('/formemployee','EmployeeController@create')->name('pages.form.employee')->middleware('auth'); 
Route::post('/formemployee','EmployeeController@store')->name('pages.form.employee')->middleware('auth'); 
Route::get('/recruitment', 'RecruitmentController@create')->name('pages.recruitment')->middleware('auth');
Route::get('get-bank-list','RecruitmentController@getBankList')->middleware('auth');
Route::get('get-branch-list','RecruitmentController@getBranchList')->middleware('auth');
Route::post('/recruitment','RecruitmentController@store')->name('pages.recruitment')->middleware('auth');
Route::get('/tablerecruitment','RecruitmentController@index')->name('pages.tbl_recruitment')->middleware('auth');
Route::get('/tablerecruitment/cetak_pdf', 'RecruitmentController@cetak_pdf')->middleware('auth');


Route::get('/bankcode','RecruitmentController@getbank')->name('bank')->middleware('auth');
Route::post('bankcode/fetch','RecruitmentController@fetch')->name('bank.fetch')->middleware('auth');


//export dan view form to csv
Route::get('/viewpersonal/export_excel', 'PersonalController@exportcsv')->name('export_excel.excel')->middleware('auth');
Route::get('/tablerecruitment/cetak_csv', 'RecruitmentController@exportcsv')->name('export_excel.excel')->middleware('auth');





Route::resource('/testingform','testingApiController')->middleware('auth');

//Route::get('/testingform', 'SubmissionFormController@create')->name('pages.testing');
Route::get('/table/user', 'TestingApiController@dataTable')->name('table.user')->middleware('auth');

Route::post('testingform/uploud','testingApiController@uploudfile')->name('testing.uploud')->middleware('auth');


Route::get('/table/user', 'TestingApiController@dataTable')->name('table.user')->middleware('auth');


// Route::any('test', function(){
//     $trip = Trip::groupBy('user_id')
//             ->select('user_id', DB::raw('count(*) as total_trip'))
//             ->get();
    
    
//     return $trip;
// });